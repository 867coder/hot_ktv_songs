#!/usr/bin/env python
# -*- coding: utf-8 -*-
import urllib2
import re
import json

# 好樂迪國語新進歌曲排行榜URL
URL_HOT_NEW_MANDARIN = "http://www.holiday.com.tw/song/NewSong.aspx?kind=c"
dict_hot_new_mandarin = {}

response = urllib2.urlopen(URL_HOT_NEW_MANDARIN)
html = response.read()
match_song = re.findall('<b>[0-9][0-9][0-9][0-9][0-9]</b></font></td><td valign=.middle.>(.*?)</td>', html)
match_artist = re.findall('GoSearch..(.*?)....>', html)

for i in range(0, len(match_song)):
    dict_hot_new_mandarin[match_artist[i].strip()] = match_song[i].strip()
    #url = "http://ws.spotify.com/search/1/track.json?q=%s+%s" % (match_artist[i].strip(), match_song[i].strip())
    url = "http://ws.spotify.com/search/1/track.json?q=%s" % (match_song[i].strip())
    response = urllib2.urlopen(url)

    if response:
        try:
            spotify_json = json.loads(response.read())
            print "%s\t%s\t%s\t%s\t%s" % (spotify_json['tracks'][0]['name'],spotify_json['tracks'][0]['artists'][0]['name'], spotify_json['tracks'][0]['album']['name'], spotify_json['tracks'][0]['album']['href'], spotify_json['tracks'][0]['external-ids'][0]['id'])
        except:
            pass
